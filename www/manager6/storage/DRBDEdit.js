Ext.define('PVE.storage.DRBDInputPanel', {
    extend: 'PVE.panel.StorageBase',
    mixins: ['Proxmox.Mixin.CBind'],

    onlineHelp: 'pvedrbd_vm_create_via_gui',

    column1: [
	{
	    xtype: 'pveBaseStorageSelector',
	    name: 'basesel',
	    fieldLabel: gettext('Base DRBD storage'),
	    cbind: {
		disabled: '{!isCreate}',
		hidden: '{!isCreate}',
	    },
	    submitValue: false,
	},
	{
	    xtype: 'pveDrbdResSelector',
	    name: 'res_name',
	    fieldLabel: gettext('Resource'),
	    reference: 'resSelector',
	    allowBlank: false,
	},
	{
	    xtype: 'pveContentTypeSelector',
	    cts: ['images', 'rootdir'],
	    fieldLabel: gettext('Content'),
	    name: 'content',
	    value: ['images', 'rootdir'],
	    multiSelect: true,
	    allowBlank: false,
	},
    ],

});
