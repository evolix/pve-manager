

Ext.define('PVE.dc.DRBDValue', {
    extend: 'Ext.grid.GridPanel',

    alias: ['widget.pveDcDRBDValue'],
    onlineHelp: 'pvedrbd_res_create_via_gui',

    stateful: true,
    stateId: 'grid-acls',

    category: "",
    res_name: "",

    getButtonClass: function() {
        var me = this;
        const winClasses = {
            "default": ['PVE.dc.DRBDValueAdd', 'PVE.dc.DRBDValueDel'],
            "node": ['PVE.dc.DRBDNodeAdd', 'PVE.dc.DRBDNodeDel'],
        };
        const winClass = winClasses[me.category] || winClasses.default;
        return winClass;
    },

    createButtons: function(store, sm) {
        var me = this;
        return [
            new Proxmox.button.Button({
                text: gettext('Add '),
                iconCls: 'fa fa-fw fa-group',
                handler: function() {
                    var win = Ext.create(me.getButtonClass()[0], { category: me.category });
                    win.on('destroy', () => store.load());
                    win.show();
                },
            }),
            new Proxmox.button.Button({
                text: gettext('Remove '),
                disabled: true,
                selModel: sm,
                handler: function(btn, event, rec) {
                    var param = rec.data;
                    var button_class = me.getButtonClass()[1];
                    var win = Ext.create(button_class, { param: param, category: me.category });
                    win.on('destroy', () => store.load());
                    win.show();
                },
            }),
        ];
    },

    updateColumns: function(store, res, cat) {
        var me = this;
        store.getProxy().setUrl("/api2/json/drbd/file/category/" + res);
        store.getProxy().setExtraParams({ category: cat });
        store.load({
            callback: function(records, operation, success) {
                let columns = [];
                if (success && records && records.length > 0) {
                    let firstRecord = records[0].data;
                    let keys = Object.keys(firstRecord);
                    let fields = keys.map(function(key) {
                        return { name: key };
                    });
                    Ext.define('pve-drbd-value', {
                        extend: 'Ext.data.Model',
                        fields: fields,
                    });
                    store.setModel('pve-drbd-value');

                    keys.forEach(function(key) {
                        if (key !== "id") {
                            columns.push({
                                header: gettext(key.charAt(0).toUpperCase() + key.slice(1)),
                                flex: 1,
                                sortable: true,
                                dataIndex: key,
                            });
                        }
                    });
                    columns.sort((a, b) => a.header - b.header);
                }
                me.reconfigure(store, columns);
            },
        });
    },

    initComponent: function() {
        let me = this;
        let sm = Ext.create('Ext.selection.RowModel', {});

        let resSelector, categorySelector;

        let store = Ext.create('Ext.data.Store', { proxy: { type: 'proxmox' } });

        let [add_btn, remove_btn] = me.createButtons(store, sm);

        resSelector = {
            xtype: 'pveDrbdResSelector',
            autoSelect: true,
            listeners: {
                change: function(f, value) {
                    me.res_name = value;
                    me.updateColumns(store, value, me.category);
                },
            },
        };

        categorySelector = {
            xtype: 'pveDrbdCategorySelector',
            editable: true,
            autoSelect: true,
            listeners: {
                change: function(f, value) {
                    me.category = value;
                    me.updateColumns(store, me.res_name, value);
                },
                expand: function(component) {
                    let compStore = component.getStore();
                    if (compStore) {
                        compStore.getProxy().url = "/api2/json/drbd/file/categories";
                        compStore.getProxy().setExtraParams({ 'res_name': me.res_name });
                        compStore.load({});
                    }
                },
            },
        };
        Proxmox.Utils.monStoreErrors(me, store);


        Ext.apply(me, {
            store: store,
            selModel: sm,
            tbar: [
                resSelector,
                categorySelector,
                add_btn,
                remove_btn,
            ],
            viewConfig: {
            trackOver: false,
            },
        });

        let sp = Ext.state.Manager.getProvider();
        let savedState = sp.get('proxmoxDrbdCategorySelection');

        if (savedState) {
            this.category = savedState.category;
        }
        me.callParent();
    },
}, function() {
    Ext.define('pve-drbd-value', {
	extend: 'Ext.data.Model',
	fields: [],
    idProperty: 'id',
    });
});

