Ext.define('PVE.dc.DRBDNodeDel', {
    extend: 'Proxmox.window.Edit',
    alias: ['widget.pveDRBDNodesDel'],
    onlineHelp: 'pvedrbd_res_create_via_gui',

    method: 'DELETE',
    isRemove: true,
    isCreate: true,
    width: 400,


	apiCallDone: function(success, response, options) {
		if (response.status === 200) {
			let data = response.result.data;
			if (!data) {
				Ext.Msg.alert(gettext('Execution error.'), response.result.message);
			}
			if (data.status !== 1) {
				Ext.Msg.alert(gettext('Can\'t save new config'), data.message);
			}
		} else {
			Ext.Msg.alert(gettext('Error'), response.status);
		}
	},

    initComponent: function() {
        let me = this;

		let sp = Ext.state.Manager.getProvider();
		let stateinit = sp.get('proxmoxDrbdResSelection');
		if (stateinit) {
			me.res_name = stateinit.name;
		}
		let items = [];

		items.push(
			{
				xtype: 'pveNodeSelector',
				allowBlank: false,
				emptyText: gettext("Select node"),
				fieldLabel: gettext("Node name"),
				// autoSelect: true,
				value: me.param.on,
				deleteEmpty: true,
				skipEmptyText: true,
				// nodename: 'stock-pve-2',
				name: 'node',
			},
			{
				xtype: 'checkbox',
				name: 'delete',
				fieldLabel: gettext('Delete LV'),
				inputValue: 1,
				uncheckedValue: 0,
				checked: false,
			},
		);

	let ipanel = Ext.create('Proxmox.panel.InputPanel', {
	    items: items,
	    onlineHelp: 'pveum_permission_management',
	});

	Ext.apply(me, {
	    items: [ipanel],
		url: '/drbd/node',
	});
	if (me.res_name) {
		me.url += "/" + me.res_name;
	}

	me.callParent();
    },
});


Ext.define('PVE.dc.DRBDNodeAdd', {
    extend: 'Proxmox.window.Edit',
    alias: ['widget.pveDRBDNodesAdd'],
    onlineHelp: 'pvedrbd_res_create_via_gui',

    method: 'PUT',
    isAdd: true,
    isCreate: true,

    width: 400,


	apiCallDone: function(success, response, options) {
		if (response.status === 200) {
			let data = response.result.data;
			if (!data) {
				Ext.Msg.alert(gettext('Execution error.'), response.result.message);
			}
			if (data.status !== 1) {
				Ext.Msg.alert(gettext('Can\'t save new config'), data.message);
			}
		} else {
			Ext.Msg.alert(gettext('Error'), response.status);
		}
	},

    initComponent: function() {
        let me = this;

		let sp = Ext.state.Manager.getProvider();
		let stateinit = sp.get('proxmoxDrbdResSelection');
		if (stateinit) {
			me.res_name = stateinit.name;
		}
		let items = [];

		items.push(
			{
				xtype: 'pveNodeSelector',
				allowBlank: false,
				emptyText: gettext("Select node"),
				fieldLabel: gettext("Node name"),
				autoSelect: false,
				deleteEmpty: true,
				skipEmptyText: true,
				name: 'node',
			},
			{
				xtype: 'checkbox',
				name: 'create',
				fieldLabel: gettext('Create LV'),
				inputValue: 1,
				uncheckedValue: 0,
				checked: false,
			},
		);

	let ipanel = Ext.create('Proxmox.panel.InputPanel', {
	    items: items,
	    onlineHelp: 'pveum_permission_management',
	});

	Ext.apply(me, {
	    items: [ipanel],
		url: '/drbd/node',
	});
	if (me.res_name) {
		me.url += "/" + me.res_name;
	}

	me.callParent();
    },
});

Ext.define('PVE.dc.DRBDValueAdd', {
    extend: 'Proxmox.window.Edit',
    alias: ['widget.pveDRBDValueAdd'],
    onlineHelp: 'pvedrbd_res_create_via_gui',

    method: 'POST',
    isAdd: true,
    isCreate: true,

    width: 400,

	apiCallDone: function(success, response, options) {
		if (response.status === 200) {
			let data = response.result.data;
			if (!data) {
				Ext.Msg.alert(gettext('Execution error.'), response.result.message);
			}
			if (data.status !== 1) {
				Ext.Msg.alert(gettext('Can\'t save new config'), data.message);
			}
		} else {
			Ext.Msg.alert(gettext('Error'), response.status);
		}
	},

    initComponent: function() {
        let me = this;

		let items = [];

		let sp = Ext.state.Manager.getProvider();
		let stateinit = sp.get('proxmoxDrbdResSelection');
		if (stateinit) {
			me.res_name = stateinit.name;
		}

		items.push({
			xtype: 'proxmoxtextfield',
			name: 'category',
            value: me.category,
            editable: false,
			fieldLabel: gettext('category'),
		});

		items.push({
			xtype: 'proxmoxtextfield',
			name: 'value',
			fieldLabel: gettext('value'),
		});

		let ipanel = Ext.create('Proxmox.panel.InputPanel', {
			items: items,
			onlineHelp: 'pveum_permission_management',
		});

		Ext.apply(me, {
			items: [ipanel],
			url: '/drbd/file/category',
		});
		if (me.res_name) {
			me.url += "/" + me.res_name;
		}

		me.callParent();
    },
});

Ext.define('PVE.dc.DRBDValueDel', {
    extend: 'Proxmox.window.Edit',
    alias: ['widget.pveDRBDValueDel'],
    onlineHelp: 'pvedrbd_res_create_via_gui',

    isRemove: true,
    isCreate: true,
    method: 'DELETE',
    width: 400,

	apiCallDone: function(success, response, options) {
		if (response.status === 200) {
			let data = response.result.data;
			if (!data) {
				Ext.Msg.alert(gettext('Execution error.'), response.result.message);
			}
			if (data.status !== 1) {
				Ext.Msg.alert(gettext('Can\'t save new config'), data.message);
			}
		} else {
			Ext.Msg.alert(gettext('Error'), response.status);
		}
	},

    initComponent: function() {
        let me = this;

		let items = [];

		let sp = Ext.state.Manager.getProvider();
		let stateinit = sp.get('proxmoxDrbdResSelection');
		if (stateinit) {
			me.res_name = stateinit.name;
		}
		items.push({
			xtype: 'proxmoxtextfield',
			name: 'category',
            value: me.category,
            editable: false,
			fieldLabel: gettext('category'),
		});

		items.push({
			xtype: 'proxmoxtextfield',
			name: 'value',
            value: me.param.value,
            editable: false,
			fieldLabel: gettext('value'),
		});

		let ipanel = Ext.create('Proxmox.panel.InputPanel', {
			items: items,
			onlineHelp: 'pveum_permission_management',
		});

		Ext.apply(me, {
			items: [ipanel],
			url: '/drbd/file/category',
		});

		if (me.res_name) {
			me.url += "/" + me.res_name;
		}

		me.callParent();
    },
});
