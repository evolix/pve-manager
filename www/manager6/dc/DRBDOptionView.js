
Ext.define('PVE.dc.DRBDOptionEdit', {
    extend: 'Proxmox.window.Edit',
    alias: ['widget.pveDRBDOptionEdit'],
    onlineHelp: 'chapter_pvedrbd',

    method: 'POST',
    width: 400,
    subject: gettext('Change settings'),

	apiCallDone: function(success, response, options) {
		if (response.status === 200) {
			let data = response.result.data;
			if (data.status !== 1) {
				Ext.Msg.alert(gettext('Error'), data.message);
			}
		} else {
			Ext.Msg.alert(gettext('Error'), response.status);
		}
	},

    initComponent: function() {
        let me = this;

		let items = [];

		let oldvalue = me.params.value;

		items.push({
			xtype: 'proxmoxtextfield',
			name: 'newvalue',
            value: oldvalue,
            editable: true,
			fieldLabel: me.params.name,
		},
		{
			xtype: 'proxmoxtextfield',
			name: 'option',
            value: me.params.name,
			hidden: true,
		});

		let ipanel = Ext.create('Proxmox.panel.InputPanel', {
			items: items,
			onlineHelp: 'pveum_permission_management',
		});

		Ext.apply(me, {
			items: [ipanel],
			url: '/drbd/settings',
		});

		me.callParent();
    },
});

Ext.define('PVE.dc.pveDRBDOptionView', {
    extend: 'Proxmox.grid.ObjectGrid',
    alias: ['widget.pveDRBDOptionView'],

    onlineHelp: 'pvedrbd_res_create_via_gui',

	reloadStore: function() {
        var me = this;
        me.rstore.stopUpdate();
        me.rstore.load({
            callback: function() {
                me.rstore.startUpdate();
            },
        });
    },

    initComponent: function() {
		var me = this;

		me.rstore = Ext.create('Proxmox.data.ObjectStore', {
			interval: 3000,
			model: 'pve-drbd-file',
			proxy: {
				type: 'proxmox',
				url: '/api2/json/drbd/settings',
			},
			sorters: {
				property: 'option',
				direction: 'ASC',
			},
		});

		let sm = Ext.create('Ext.selection.RowModel', {});

		Proxmox.Utils.monStoreErrors(me, me.rstore);

		let editbtn = new Proxmox.button.Button({
			text: gettext('Edit'),
			disabled: true,
			selModel: sm,
			handler: function(btn, event, rec) {
				var opt = rec.data.option;
				var value = rec.data.value;
				var win = Ext.create('PVE.dc.DRBDOptionEdit', { params: { name: opt, value: value } });
				win.show();
				win.on('destroy', me.reloadStore);
			},
		});

		Ext.apply(me, {
			store: me.rstore,
			stateful: false,
			selModel: sm,
			viewConfig: {
				trackOver: false,
			},
			tbar: [
				editbtn,
			],
			columns: [
			{
				header: gettext('Name'),
				flex: 1,
				dataIndex: 'option',
			},
			{
				header: gettext('Value'),
				flex: 2,
				dataIndex: 'value',
			},
			],
		});

		me.callParent();

		me.on('activate', me.rstore.startUpdate);
		me.on('destroy', me.rstore.stopUpdate);
		me.rstore.startUpdate();
    },
}, function() {
    Ext.define('pve-drbd-settings', {
		extend: 'Ext.data.Model',
		fields: [
			'option', 'value',
		],
		idProperty: 'option',
    });
});
