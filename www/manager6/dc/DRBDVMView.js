Ext.define('PVE.dc.DRBDVMView', {
    extend: 'Ext.grid.GridPanel',
    alias: ['widget.pveDRBDVMView'],

    initComponent: function() {
		var me = this;

		let sm = Ext.create('Ext.selection.RowModel', {});

		me.rstore = Ext.create('Proxmox.data.ObjectStore', {
			interval: 30000,
			model: 'pve-drbd-vm',
			proxy: {
				type: 'proxmox',
				url: '/api2/json/drbd/vm',
			},
			sorters: {
				property: 'vmid',
				direction: 'ASC',
			},
		});

		Proxmox.Utils.monStoreErrors(me, me.rstore);

		Ext.apply(me, {
			store: me.rstore,
			selModel: sm,
			stateful: false,
			viewConfig: {
			trackOver: false,
			},
			columns: [
			{
				header: gettext('VMID'),
                flex: 1,
				dataIndex: 'vmid',
			},
			{
				header: gettext('Resource(s)'),
				flex: 1,
				dataIndex: 'resources',
			},
			{
				header: gettext('Storage(s)'),
				width: 80,
				flex: 1,
				dataIndex: 'storages',
			},
			{
				header: gettext('Node'),
				width: 80,
				flex: 1,
				dataIndex: 'node',
			},
			],
		});

		Ext.apply(me, {
			tbar: [],
		});

		me.callParent();

		me.on('activate', me.rstore.startUpdate);
		me.on('destroy', me.rstore.stopUpdate);
		me.rstore.startUpdate();
    },
}, function() {
    Ext.define('pve-drbd-vm', {
	extend: 'Ext.data.Model',
	fields: [
	    'resources', 'vmid', 'storages', 'message',
	],
	idProperty: 'vmid',
    });
});
