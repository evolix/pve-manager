Ext.define('PVE.form.DrbdNodeSelector', {
    extend: 'Proxmox.form.ComboGrid',
    alias: 'widget.drbdNodeSelector',
    mixins: ['Proxmox.Mixin.CBind'],
    onlineHelp: 'pvedrbd_res_create_via_gui',

    allowBlank: false,
    valueField: 'node',
    displayField: 'node',
    listConfig: {
	width: 450,
	columns: [
	    {
		header: gettext('Name'),
		dataIndex: 'node',
		hideable: false,
		flex: 1,
	    },
	],
    },

	resname: undefined,

    initComponent: function() {
        var me = this;

        if (!me.resname) {
            me.resname = "";
        }

        var store = Ext.create('Ext.data.Store', {
            model: 'pve-storage-node',
            proxy: {
                type: 'proxmox',
                url: 'api2/json/drbd/node/' + me.resname,
            },
            sorters: {
                property: 'node',
                direction: 'ASC',
            },
        });

        Ext.apply(me, {
            store: store,
        });
        store.load();
        me.callParent();
    },
}, function() {
    Ext.define('pve-storage-node', {
	extend: 'Ext.data.Model',
	fields: ['node', 'address'],
	idProperty: 'node',
    });
});

Ext.define('PVE.form.LVMSelector', {
    extend: 'Proxmox.form.ComboGrid',
    alias: 'widget.drbdLVMSelector',
    mixins: ['Proxmox.Mixin.CBind'],

    allowBlank: false,
    valueField: 'path',
    displayField: 'name',

	selectNode: 'undef',

	store: {
	    fields: ['name', 'vg', 'size', 'path'],
		proxy: {
			type: 'proxmox',
			url: `/api2/json/drbd/lvs`,
		},
		sorters: {
			property: 'name',
			direction: 'ASC',
		},
	},

	listConfig: {
		columns: [
			{
				header: gettext('Name'),
				dataIndex: 'name',
				flex: 1,
			},
			{
				header: gettext('VG'),
				dataIndex: 'vg',
				flex: 1,
			},
			{
				header: gettext('Size'),
				dataIndex: 'size',
				flex: 1,
			},
			{
				header: gettext('Path'),
				dataIndex: 'path',
				flex: 1,
			},
		],
    },

	changeNode(node) {
		var me = this;

		me.selectNode = node;
		me.getStore().getProxy().setExtraParam("node", node);
		me.getStore().load();
	},

    initComponent: function() {
		var me = this;

		me.callParent();
		me.changeNode(me.selectNode);
		me.getStore().load();
    },

}, function() {
    Ext.define('pve-lvm-status', {
	extend: 'Ext.data.Model',
	fields: ['storage'],
	idProperty: 'storage',
    });
});

Ext.define('PVE.form.DRBDStatusSelector', {
    extend: 'Proxmox.form.KVComboBox',
    alias: 'widget.pveDRBDStatusSelector',
    comboItems: [
	['primary', 'Primary'],
	['secondary', 'Secondary'],
	['primary --force', 'Primary (force)'],

    ],
});

Ext.define('Proxmox.form.DrbdResSelector', {
    extend: 'Ext.form.field.ComboBox',
    alias: ['widget.pveDrbdResSelector'],

    displayField: 'name',
    valueField: 'name',
    editable: false,
    queryMode: 'local',
    stateEvents: ['select'],
    stateful: true,
    stateId: 'proxmoxDrbdResSelection',

    //Select res based on selModel for example
    selectRes: undefined,

    // save current selection in the state Provider so RRDView can read it
    getState: function() {
    let ind = this.getStore().findExact('name', this.getValue());
	let rec = this.getStore().getAt(ind);
	if (!rec) {
	    return undefined;
	}
	return {
	    path: rec.data.path,
	    status: rec.data.status,
        name: rec.data.name,
        id: rec.data.id,
	};
    },
    // set selection based on last saved state
    applyState: function(state) {
        if (state && state.name) {
            this.setValue(state.name);
        }
    },

    initComponent: function() {
        let me = this;
        let store = Ext.create('Ext.data.Store', {
            model: 'pve-drbd-res-file',
            proxy: {
                type: 'proxmox',
                url: "/api2/json/drbd/file",
                sorters: [{
                    property: 'name',
                    direction: 'ASC',
                }],
                sortRoot: 'name',
                sortOnLoad: true,
            },
        });

        Proxmox.Utils.monStoreErrors(me, store);

        Ext.apply(me, {
            store: store,
        });
        store.load({
                callback: function (records){
                    if (me.autoSelect && records) {
                        records.forEach(resource => {
                            if (!me.getState()) {
                                me.setValue(resource.data.name);
                            }
                        });
                    }
                },
            },
        );
        me.on('afterrender', function() {
            if (me.selectRes) {
                me.setValue(me.selectRes);
            }
        });
        me.callParent();
    },

}, function() {
    Ext.define('pve-drbd-res-file', {
	extend: 'Ext.data.Model',
    fields: ["path", "status", "name", "id"],
    type: 'array',
    });
});

Ext.define('PVE.form.DrbdVMSelector', {
    extend: 'Proxmox.form.ComboGrid',
    alias: ['widget.pveDrbdVMSelector'],

    selectCurNode: false,
    autoSelect: false,

    valueField: 'vmid',
    displayField: 'vmid',
    store: {
	    fields: ['node', 'vmid'],
	    proxy: {
		type: 'proxmox',
		url: '/api2/json/drbd/utils/vms',
	    },
	    sorters: [
		{
		    property: 'node',
		    direction: 'ASC',
		},
	    ],
	},

    listConfig: {
	columns: [
	    {
		header: gettext('VMID'),
		dataIndex: 'vmid',
		sortable: true,
		hideable: false,
		flex: 1,
	    },
	    {
		header: gettext('Node'),
		sortable: true,
		width: 100,
		dataIndex: 'node',
	    },
	],
    },
    initComponent: function() {
	var me = this;
	me.callParent();
	me.getStore().load();

	me.mon(me.getStore(), 'load', () => me.isValid());
    },
});


Ext.define('Proxmox.form.DrbdCategorySelector', {
    extend: 'Ext.form.field.ComboBox',
    alias: ['widget.pveDrbdCategorySelector'],

    displayField: 'category',
    valueField: 'category',
    queryMode: 'local',
    editable: true,
    stateEvents: ['wawa'],
    stateful: true,
    stateId: 'proxmoxDrbdCategorySelection',


    // save current selection in the state Provider so RRDView can read it
    getState: function() {
        let ind = this.getStore().findExact('category', this.getValue());
        let rec = this.getStore().getAt(ind);
        if (!rec) {
            return undefined;
        }
        return {
            category: rec.data.category,
        };
    },
    // set selection based on last saved state
    applyState: function(state) {
        if (state && state.category) {
            this.setValue(state.category);
        }
    },
    initComponent: function() {
        let me = this;

        let store = Ext.create('Ext.data.Store', {
            model: 'pve-drbd-category-file',
            proxy: {
                type: 'proxmox',
                url: "/api2/json/drbd/file/categories",
                sorters: [{
                    property: 'category',
                    direction: 'ASC',
                }],
                sortRoot: 'category',
                sortOnLoad: true,
            },
        });

        let provider = Ext.state.Manager.getProvider();
        let resSelection = provider.get('proxmoxDrbdResSelection');
        if (resSelection) {
            store.getProxy().setExtraParams({ res_name: resSelection.name });
        }

        Proxmox.Utils.monStoreErrors(me, store);

        Ext.apply(me, {
            store: store,
        });
        store.load({
            callback: function (records){
                if (me.autoSelect && records) {
                    records.forEach(resource => {
                        if (!me.getState()) {
                            me.setValue(resource.data.category);
                        }
                    });
                }
             },
        });
        me.callParent();
        },

}, function() {
    Ext.define('pve-drbd-category-file', {
	extend: 'Ext.data.Model',
    fields: ["category"],
    type: 'array',
    });
});
