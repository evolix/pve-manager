Ext.define('PVE.dc.DRBDResStart', {
    extend: 'Proxmox.window.Edit',
    alias: ['widget.pveDRBDResStart'],
    onlineHelp: 'pvedrbd_res_create_via_gui',

    url: '/drbd/resource/start',
    method: 'GET',
    isCreate: true,
	isReset: true,

    width: 400,

	apiCallDone: function(success, response, options) {
		if (response.status === 200) {
			let data = response.result.data;
			if (data.status !== 1) {
				Ext.Msg.alert(gettext('Can\'t start resource'), data.message);
			}
		} else {
			Ext.Msg.alert(gettext('Error'), response.status);
		}
	},

    initComponent: function() {
        let me = this;

	    me.subject = gettext("Start DRBD Ressource");

		let current_node = "";
		if (me.params) {
			current_node = me.params.node;
		}

		let items = [];
		items.push({
			xtype: 'pveDrbdResSelector',
			allowBlank: false,
			emptyText: gettext("Select res"),
			fieldLabel: gettext("Ressource name"),
			deleteEmpty: true,
			skipEmptyText: true,
			name: 'res_name',
		},
		{
			xtype: 'checkbox',
			name: 'restrict',
			fieldLabel: gettext(current_node + ' only'),
			inputValue: 1,
			uncheckedValue: 0,
			checked: false,
		},
		{
			xtype: 'proxmoxtextfield',
			name: 'node',
            value: current_node,
			hidden: true,
		});

		let ipanel = Ext.create('Proxmox.panel.InputPanel', {
			items: items,
			onlineHelp: 'pveum_permission_management',
		});

		Ext.apply(me, {
			items: [ipanel],
		});

		me.callParent();
    },

});

Ext.define('PVE.dc.DRBDResStop', {
    extend: 'Proxmox.window.Edit',
    alias: ['widget.pveDRBDResStop'],

    url: '/drbd/resource/stop',
    method: 'GET',
    isCreate: true,
	isReset: true,
    width: 400,

	apiCallDone: function(success, response, options) {
		if (response.status === 200) {
			let data = response.result.data;
			if (data.status !== 1) {
				Ext.Msg.alert(gettext('Can\'t stop resource'), data.message);
			}
		} else {
			Ext.Msg.alert(gettext('Error'), response.status);
		}
	},
    initComponent: function() {
        let me = this;

	    me.subject = gettext("Stop DRBD Ressource");

		let current_node = "";
		if (me.params) {
			current_node = me.params.node;
		}

		let items = [];
		items.push({
			xtype: 'pveDrbdResSelector',
			allowBlank: false,
			emptyText: gettext("Select res"),
			fieldLabel: gettext("Ressource name"),
			selectRes: me.params.res_name,
			deleteEmpty: true,
			skipEmptyText: true,
			name: 'res_name',
		},
		{
			xtype: 'checkbox',
			name: 'restrict',
			fieldLabel: gettext(current_node + ' only'),
			inputValue: 1,
			uncheckedValue: 0,
			checked: false,
		},
		{
			xtype: 'proxmoxtextfield',
			name: 'node',
            value: current_node,
			hidden: true,
		});

		let ipanel = Ext.create('Proxmox.panel.InputPanel', {
			items: items,
			onlineHelp: 'pveum_permission_management',
		});

		Ext.apply(me, {
			items: [ipanel],
		});

		me.callParent();
    },
});


Ext.define('PVE.dc.DRBDResChangeStatus', {
    extend: 'Proxmox.window.Edit',
    alias: ['widget.pveDRBDResChangeStatus'],
    onlineHelp: 'pvedrbd_res_create_via_gui',

    url: '/drbd/resource/status_change',
    method: 'GET',
    isAdd: true,
    isCreate: true,

    width: 400,

	apiCallDone: function(success, response, options) {
		if (response.status === 200) {
			let data = response.result.data;
			if (data.status !== 1) {
				Ext.Msg.alert(gettext('Error'), data.message);
			}
		} else {
			Ext.Msg.alert(gettext('Error'), response.status);
		}
	},

	refreshNodeSelector: function(res_name) {
        let nodeSelector = this.down('drbdNodeSelector');
		nodeSelector.value = "";
        if (nodeSelector) {
            // Fetch the nodes based on the selected resource ID
            nodeSelector.getStore().getProxy().setUrl("api2/json/drbd/node/" + res_name);
			nodeSelector.getStore().load();
        }
    },

    initComponent: function() {
        let me = this;

		me.subject = gettext("Change node statut");

		let items = [];
		items.push({
			xtype: 'pveDrbdResSelector',
			allowBlank: false,
			emptyText: gettext("Select res"),
			fieldLabel: gettext("Ressource name"),
			deleteEmpty: true,
			skipEmptyText: true,
			selectRes: me.params.res_name,
			url: 'api2/json/drbd/node/',
			name: 'res_name',
			listeners: {
                change: function(f, value) {
                    me.refreshNodeSelector(value);
                },
            },
		});
		items.push({
			xtype: 'drbdNodeSelector',
			allowBlank: false,
			emptyText: gettext(""),
			fieldLabel: gettext("Node to change"),
			deleteEmpty: true,
			skipEmptyText: true,
			resname: me.params.res_name,
			name: 'node',
		});
		items.push({
			xtype: 'pveDRBDStatusSelector',
			allowBlank: false,
			emptyText: gettext(""),
			fieldLabel: gettext("New status"),
			autoSelect: true,
			deleteEmpty: true,
			skipEmptyText: true,
			name: 'status',
		});
		let ipanel = Ext.create('Proxmox.panel.InputPanel', {
			items: items,
			onlineHelp: 'pveum_permission_management',
		});

		Ext.apply(me, {
			items: [ipanel],
		});

		me.callParent();
    },

});

Ext.define('PVE.dc.DRBDStatusView', {
    extend: 'Ext.grid.GridPanel',
    alias: ['widget.pveDRBDStatusView'],

    onlineHelp: 'pvedrbd_res_create_via_gui',

	current_node: "",
	reloadStore: function() {
        var me = this;
        me.rstore.stopUpdate();
        me.rstore.load({
            callback: function() {
                me.rstore.startUpdate();
            },
        });
    },

    initComponent: function() {
		var me = this;

		let sm = Ext.create('Ext.selection.RowModel', {});

		me.rstore = Ext.create('Proxmox.data.ObjectStore', {
			interval: 3000,
			model: 'pve-drbd-status',
			proxy: {
				type: 'proxmox',
				url: '/api2/json/drbd/resource',
				extraParams: {
					node: 'default',
				},
			},
			sorters: {
				property: 'res_name',
				direction: 'ASC',
			},
		});

		Proxmox.Utils.monStoreErrors(me, me.rstore);

		let changeStatusButon = new Proxmox.button.Button({
            text: gettext('Change Status'),
			iconCls: 'fa fa-fw fa-group',
            disabled: true,
            selModel: sm,
            handler: function(btn, event, rec) {
				var win = Ext.create('PVE.dc.DRBDResChangeStatus', { params: rec.data });
				win.show();
            },
        });
		let stopRes = new Proxmox.button.Button({
			text: gettext('Stop Res'),
            disabled: true,
            selModel: sm,
            handler: function(btn, event, rec) {
				var win = Ext.create('PVE.dc.DRBDResStop', { params: { res_name: rec.data.res_name, node: me.current_node } });
				win.show();
            },
        });
		let startRes = new Proxmox.button.Button({
			text: gettext('Start Res'),
            handler: function(btn, event, rec) {
				var win = Ext.create('PVE.dc.DRBDResStart', { params: { node: me.current_node } });
				win.show();
            },
        });

		Ext.apply(me, {
			store: me.rstore,
			selModel: sm,
			stateful: false,
			viewConfig: {
				trackOver: false,
			},
			columns: [
			{
				header: gettext('Name'),
				width: 80,
				flex: 1,
				dataIndex: 'res_name',
			},
			{
				header: gettext('Connection'),
				width: 80,
				flex: 1,
				dataIndex: 'connection',
			},
			{
				header: gettext('Roles'),
				width: 80,
				flex: 1,
				dataIndex: 'roles',
			},
			{
				header: gettext('Storages'),
				width: 80,
				flex: 1,
				dataIndex: 'storages',
			},
			],
			tbar: [
				startRes,
				stopRes,
				changeStatusButon,
				{
					xtype: 'pveNodeSelector',
					// fieldLabel: gettext('Host'),
					selectCurNode: true,
					allowBlank: false,
					submitValue: false,
					listeners: {
						change: function(f, value) {
							me.current_node = value;
							me.rstore.getProxy().setExtraParam('node', value);
							me.reloadStore();
						},
					},
				},
			],
		});

		me.callParent();

		me.on('activate', me.rstore.startUpdate);
		me.on('destroy', me.rstore.stopUpdate);
		me.rstore.startUpdate();
    },
}, function() {
    Ext.define('pve-drbd-status', {
	extend: 'Ext.data.Model',
	fields: [
	    'res_name', 'type', 'roles', 'status', 'storages', 'message',
	],
	idProperty: 'res_name',
    });
});
