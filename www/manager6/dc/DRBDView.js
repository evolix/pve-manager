Ext.define('PVE.dc.DRBDMigrate', {
    extend: 'Proxmox.window.Edit',
    alias: ['widget.pveDRBDMigrate'],
	onlineHelp: 'pvedrbd_hot_migration_gui',
	method: 'PUT',
	url: '/drbd/migrate',
    isAdd: true,
    isCreate: true,

    width: 400,

	extraRequestParams: {
		src_node: "",
	},

	apiCallDone: function(success, response, options) {
		if (response.status === 200) {
			let data = response.result.data;
			if (!data) {
				Ext.Msg.alert(gettext('Execution error.'), response.result.message);
			}
			if (data.status !== 1) {
				Ext.Msg.alert(gettext('Can\'t migrate'), data.message);
			}
		} else {
			Ext.Msg.alert(gettext('Error'), response.status);
		}
	},

    initComponent: function() {
        let me = this;

		let items = [];

	    me.subject = gettext("Add DRBD Node");

		items.push(
			{
				xtype: 'pveDrbdVMSelector',
				allowBlank: false,
				emptyText: gettext("Select VM"),
				fieldLabel: gettext("VMID"),
				deleteEmpty: true,
				skipEmptyText: true,
				name: 'vmid',
				listeners: {
					select: function(combo, record, index) {
						if (record.data) {
							me.extraRequestParams.src_node = record.data.node;
						}
					},
				},
			},
			{
				xtype: 'pveNodeSelector',
				allowBlank: false,
				emptyText: gettext("Select node"),
				fieldLabel: gettext("Destination node"),
				autoSelect: false,
				deleteEmpty: true,
				skipEmptyText: true,
				name: 'dst_node',
			},
			{
				xtype: 'checkbox',
				name: 'create',
				fieldLabel: gettext('Create LV'),
				inputValue: 1,
				uncheckedValue: 0,
				checked: false,
			},
		);

	let ipanel = Ext.create('Proxmox.panel.InputPanel', {
	    items: items,
	    onlineHelp: 'pveum_permission_management',
	});

	Ext.apply(me, {
	    items: [ipanel],
	});

	me.callParent();
    },
});

Ext.define('PVE.dc.DRBDDanse', {
    extend: 'Proxmox.window.Edit',
    alias: ['widget.pveDRBDDanse'],
	onlineHelp: 'pvedrbd_danse_gui',
	method: 'PUT',
	url: '/drbd/migrate/danse',
    isAdd: true,
    isCreate: true,

    width: 400,

	apiCallDone: function(success, response, options) {
		if (response.status === 200) {
			let data = response.result.data;
			if (!data) {
				Ext.Msg.alert(gettext('Execution error.'), response.result.message);
			}
			if (data.status !== 1) {
				Ext.Msg.alert(gettext('Can\'t danse'), data.message);
			}
		} else {
			Ext.Msg.alert(gettext('Error'), response.status);
		}
	},

    initComponent: function() {
        let me = this;

		let items = [];

		items.push(
			{
				xtype: 'pveDrbdVMSelector',
				allowBlank: false,
				emptyText: gettext("Select VM"),
				fieldLabel: gettext("VMID"),
				deleteEmpty: true,
				skipEmptyText: true,
				name: 'vmid',
			},
			{
				xtype: 'pveNodeSelector',
				allowBlank: false,
				emptyText: gettext("Select node"),
				fieldLabel: gettext("New primary"),
				autoSelect: false,
				deleteEmpty: true,
				skipEmptyText: true,
				name: 'first_node',
			},
			{
				xtype: 'pveNodeSelector',
				allowBlank: false,
				emptyText: gettext("Select node"),
				fieldLabel: gettext("New secondary"),
				autoSelect: false,
				deleteEmpty: true,
				skipEmptyText: true,
				name: 'second_node',
			},
			{
				xtype: 'checkbox',
				name: 'create',
				fieldLabel: gettext('Create LV'),
				inputValue: 1,
				uncheckedValue: 0,
				checked: false,
			},
		);

	let ipanel = Ext.create('Proxmox.panel.InputPanel', {
	    items: items,
	    onlineHelp: 'pveum_permission_management',
	});

	Ext.apply(me, {
	    items: [ipanel],
	});

	me.callParent();
    },
});


Ext.define('PVE.dc.DRBDView', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.pveDcDRBDView',

    onlineHelp: 'pvedrbd_how_drbd_works',
    layout: {
	type: 'vbox',
	align: 'stretch',
    },

    initComponent: function() {
	var me = this;


	let items = [];

	items = [
	{
		xtype: 'pveDRBDVMView',
		title: gettext('VM'),
		border: 0,
		collapsible: true,
		padding: '0 0 20 0',
	},
	{
		xtype: 'pveDRBDFileView',
		flex: 1,
		collapsible: true,
		title: gettext('Resources'),
		border: 0,
	},
	{
	    xtype: 'pveDRBDStatusView',
	    title: gettext('Status'),
	    border: 0,
	    collapsible: true,
	    padding: '0 0 20 0',
	},
	];

	let ipanel = Ext.create('Proxmox.panel.InputPanel', {
		items: items,
		onlineHelp: 'pveum_permission_management',
	});

	var run_migrate = function() {
		var win = Ext.create('PVE.dc.DRBDMigrate', {});
		win.show();
	};

	var run_danse = function() {
		var win = Ext.create('PVE.dc.DRBDDanse', {});
		win.show();
	};

	Ext.apply(me, {
		tbar: [{
			text: gettext('Migrate'),
			xtype: 'proxmoxButton',
			handler: function() { run_migrate(); },
		},
		{
			text: gettext('Danse'),
			xtype: 'proxmoxButton',
			handler: function() { run_danse(); },
		}],
	    items: [ipanel],
	});

	me.callParent();

	Proxmox.Utils.API2Request({
		url: '/drbd/utils/isclean',
		method: 'GET',
		success: function(response) {
            let data = response.result.data;
			if (data.status !== 1) {
				Ext.Msg.alert(gettext('Dirty working dir'), data.message);
			}
        },
        failure: function(response) {
           console.log('Can\'t check if /etc/drbd.d dir is clean. verify that the dir exist or clean it manually (verify that every .res files are in the conf.cfg files');
        },
	});
    },
});
