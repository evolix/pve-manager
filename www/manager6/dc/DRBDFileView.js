Ext.define('PVE.dc.DRBDFileAdd', {
    extend: 'Proxmox.window.Edit',
    alias: ['widget.pveDRBDFileAdd'],
    onlineHelp: 'pvedrbd_res_create_via_gui',

    url: '/drbd/file/create',
    method: 'PUT',
    isAdd: true,
    isCreate: true,

    width: 400,


	apiCallDone: function(success, response, options) {
		if (response.status === 200) {
			let data = response.result.data;
			if (!data) {
				Ext.Msg.alert(gettext('Execution error.'), response.result.message);
			}
			if (data.status !== 1) {
				Ext.Msg.alert(gettext('Error'), data.message);
			}
		} else {
			Ext.Msg.alert(gettext('Error'), response.status);
		}
	},

	refreshLVMSelector: function(node) {
        let LVMSelector = this.down('drbdLVMSelector');
        if (LVMSelector) {
            LVMSelector.changeNode(node);
        }
    },

    initComponent: function() {
        let me = this;

		let items = [];

	    me.subject = gettext("Add DRBD Node");
		items.push({
				xtype: 'proxmoxtextfield',
				name: 'name',
				fieldLabel: gettext('File name'),
			},
			{
				xtype: 'proxmoxtextfield',
				name: 'res_name',
				fieldLabel: gettext('Ressource name'),
			},
			{
				xtype: 'proxmoxtextfield',
				name: 'device',
				fieldLabel: gettext('Device'),
			},
			{
				xtype: 'pveNodeSelector',
				allowBlank: false,
				emptyText: gettext("Select node"),
				fieldLabel: gettext("First node"),
				autoSelect: false,
				deleteEmpty: true,
				skipEmptyText: true,
				name: 'node',
				listeners: {
					change: function(f, value) {
						me.refreshLVMSelector(value);
					},
					},
			},
			{
				xtype: 'drbdLVMSelector',
				name: 'disk',
				fieldLabel: gettext('Ressource disk'),
			},
			{
				xtype: 'proxmoxtextfield',
				name: 'port',
				fieldLabel: gettext('Ressource port'),
			},
		);
		let ipanel = Ext.create('Proxmox.panel.InputPanel', {
			items: items,
			onlineHelp: 'pveum_permission_management',
		});

		Ext.apply(me, {
			items: [ipanel],
		});

		me.callParent();
    },
});


Ext.define('PVE.dc.DRBDFileView', {
    extend: 'Ext.grid.GridPanel',
    alias: ['widget.pveDRBDFileView'],

    onlineHelp: 'pvedrbd_res_create_via_gui',
    sortPriority: {
	quorum: 1,
	master: 2,
	lrm: 3,
	service: 4,
    },

    initComponent: function() {
		var me = this;

		me.rstore = Ext.create('Proxmox.data.ObjectStore', {
			interval: 10000,
			model: 'pve-drbd-file',
			proxy: {
				type: 'proxmox',
				url: '/api2/json/drbd/file',
			},
			sorters: {
				property: 'name',
				direction: 'ASC',
			},
		});

		let sm = Ext.create('Ext.selection.RowModel', {});

		Proxmox.Utils.monStoreErrors(me, me.rstore);

		let remove_btn = new Proxmox.button.Button({
			text: gettext('Delete file'),
			disabled: true,
			selModel: sm,
			confirmMsg: gettext('Are you sure you want to remove this entry'),
			handler: function(btn, event, rec) {
				var params = {
					path: rec.data.path,
				};

				Proxmox.Utils.API2Request({
					url: '/drbd/file/delete',
					params: params,
					method: 'PUT',
					waitMsgTarget: me,
					success: (response, option) => {
						me.rstore.load();
						let data = response.result.data;
						if (!data) {
							Ext.Msg.alert(gettext('Execution error.'), response.result.message);
						}
						if (data.status !== 1) {
							Ext.Msg.alert(gettext('Error'), data.message);
						}
					},
					failure: response => Ext.Msg.alert(gettext('Error'), response.htmlStatus),
				});
			},
		});

		Ext.apply(me, {
			store: me.rstore,
			stateful: false,
			selModel: sm,
			viewConfig: {
			trackOver: false,
			},
			tbar: [
				{
					text: gettext('Add res file'),
					iconCls: 'fa fa-fw fa-group',
					handler: function() {
						var win = Ext.create('PVE.dc.DRBDFileAdd', {
						aclType: 'group',
						path: me.path,
						});
						win.on('destroy', () => me.rstore.load());
						win.show();
					},
				},
				remove_btn,
			],
			columns: [
			{
				header: gettext('Name'),
				width: 100,
				dataIndex: 'name',
			},
			{
				header: gettext('Device'),
				width: 100,
				dataIndex: 'device',
			},
			{
				header: gettext('Disk'),
				width: 200,
				dataIndex: 'disk',
			},
			{
				header: gettext('Status'),
				width: 200,
				dataIndex: 'status',
			},
			{
				header: gettext('Port'),
				width: 50,
				dataIndex: 'port',
			},
			{
				header: gettext('Path'),
				flex: 1,
				dataIndex: 'path',
			},
			],
		});


		me.callParent();

		me.on('activate', me.rstore.startUpdate);
		me.on('destroy', me.rstore.stopUpdate);
		me.rstore.startUpdate();
    },
}, function() {
    Ext.define('pve-drbd-file', {
	extend: 'Ext.data.Model',
	fields: [
	    'path', 'type', 'roles', 'status', 'storages',
	    'state', 'name',
	],
	idProperty: 'id',
    });
});
