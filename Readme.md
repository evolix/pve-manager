# Documentation des Modifications Proxmox pour l'Intégration de DRBD

## 1. Modifications du fork

## pve-manager:

Le paquet `pve-manager` se décompose en deux parties :
- Interface utilisateur (UI) définie dans le dossier `www/`
- Base des appels API, définie dans le dossier `PVE/`

### Interface:

Le dossier `manager6` contient tous les fichiers JavaScript.

**Fonctionnement :**
- Le dossier `node` comporte toutes les fenêtres disponibles lors de la sélection d'un nœud.
- Le dossier `storage` comporte toutes les fenêtres de création de stockage.
- Le dossier `dc` comporte toutes les fenêtres disponibles lors de la sélection du Datacenter.

**Modifications :**
- `Utils.js` : Ajout du bouton de création du stockage DRBD et définition de son type (Datacenter -> storage -> add) ainsi que des messages pour les tâches.
- `storage/DRBDEdit.js` : Fenêtre de création du stockage avec ses attributs (la ressource liée, etc.)
  **NOTE :** L'URL de l'API lors de la création du stockage n'est pas explicitement écrite. Le `DRBDInputPanel` hérite de `PVE.panel.StorageBase` qui appelle l'API en lui envoyant le type (ici drbd) qui va rediriger les appels dans le paquet `pve-storage` (voir Fonctionnement `pve-storage`).
- `dc/DRBD*.js` : Toutes les fenêtres pour DRBD (gestion des fichiers, des ressources, etc.)
- `dc/Config.js` : Ajouts des fenêtres dans le menu déroulant.

### Appels API:

**Fonctionnement :**
- `API2.pm` est le point d'entrée pour les requêtes API. On peut y trouver toutes les redirections vers les différents modules Perl. La plupart d'entre eux se trouvent dans le dossier `API2/`.
- `CLI/` comporte plusieurs CLI pour la gestion de Proxmox (`pvenode`, `pvesh`, etc.). Leur fonctionnement réside dans de simples appels API lors de l'exécution de commandes.

**Modifications :**
- `API2.pm` : Ajout de la redirection vers le fichier `API2/Drbd.pm` lors des appels API `api2/json/drbd`.
- `API2/Drbd.pm` : Redirection de toutes les requêtes DRBD vers les bons modules Perl, ainsi que quelques appels 'utils'.


## 2. Instructions d'installation

Ce paquet est essentiel pour le fonctionnement de [pve-drbd](https://gitea.evolix.org/evolix/pve-drbd.git). Il est donc inutile de l'installer seul.

1. Télécharger les sources du paquet ([pve-manager](https://gitea.evolix.org/evolix/pve-manager.git))
2. Compiler et installer avec `make dinstall` ou `gbp buildpackage` (dans la branch debian)
