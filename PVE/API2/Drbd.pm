package PVE::API2::Drbd;

use strict;
use warnings;

use PVE::RESTHandler;
use base qw(PVE::RESTHandler);

use PVE::API2::File;
use PVE::API2::File::Node;
use PVE::API2::Utils;
use PVE::API2::Utils::Migrate;
use PVE::API2::Settings;
use PVE::API2::Resource;

__PACKAGE__->register_method ({
    name => 'lv_index',
    path => 'lvs',
    method => 'GET',
    description => "Retrieve the list of logical volumes (LVs) available on the specified node.",
    permissions => {
        user => 'all',
    },
    protected => 1,
    parameters => {
        additionalProperties => 0,
        properties => {
            node => {
               description => "Node",
                type => 'string',
            },
        },
	},
    returns => {
        type => 'array',
        items => {
            type => "object",
            properties => { 
                name => {
                    type => 'string',
                },
                vg => {
                    type => 'string',
                },
                path => {
                    type => 'string',
                },
                size => {
                    type => 'string',
                },
            },
        },
    },
    code => sub {
        my ($param) = @_;
        my $ret = [];
        my $node = $param->{node};

        my $lvs_unformat = PVE::DRBD::Utils::get_lv_list($node);
        
        return $lvs_unformat->{$node};
    }
    });

__PACKAGE__->register_method ({
    name => 'vm_infos',
    path => 'vm',
    method => 'GET',
    permissions => {
        check => ['perm', '/', [ 'Sys.Audit' ]],
    },
    protected => 1,
    description => "Retrieve information about virtual machines (VMs), including their associated nodes, VM IDs, storage, and resources.",
    parameters => {
        additionalProperties => 0,
        properties => {},
    },
    returns => {
        type => 'array',
        items => {
            type => 'object',
            additionalProperties => 0,
            properties => {
                status => {
                    type => 'integer',
                    description => "status",
                },
                node => {
                    type => 'string',
                    description => "node where vm is host",
                },
                vmid => {
                    type => 'integer',
                    description => "VMID",
                },
                storages => {
                    type => 'string',
                    description => "VMID associated storages",
                },
                resources => {
                    type => 'string',
                    description => "VMID associated resources",
                },
            },
        }
    },
    code => sub {
        my $ret = [];

        my $vm_list = PVE::DRBD::Utils::get_vm_list();

        for my $vmid (keys %{$vm_list}) {
            my $diskinfo = PVE::DRBD::Utils::get_vm_storage_formated($vmid);
            my @storages = keys %{$diskinfo};

            my @vm_res;
            for my $storeid (@storages) {
                my $storageinfo = PVE::DRBD::Utils::get_storage_config($storeid);
                my $res_name = $storageinfo->{res_name};
                push @vm_res, $res_name;
            }

            my $storage_string = PVE::DRBD::Utils::array_to_string(@storages);
            my $resources_string = PVE::DRBD::Utils::array_to_string(@vm_res);

            push @$ret, {
                status => 1,
                node => $vm_list->{$vmid}->{node},
                vmid => $vmid,
                storages => $storage_string,
                resources => $resources_string,
            };
        }
        return $ret;
    }
});


__PACKAGE__->register_method ({
    subclass => "PVE::API2::Resource",
    path => 'resource',
});

__PACKAGE__->register_method ({
    subclass => "PVE::API2::File",
    path => 'file',
});


__PACKAGE__->register_method ({
    subclass => "PVE::API2::File::Node",
    path => 'node',
});

__PACKAGE__->register_method ({
    subclass => "PVE::API2::Utils::Migrate",
    path => 'migrate',
});

__PACKAGE__->register_method ({
    subclass => "PVE::API2::Settings",
    path => 'settings',
});

__PACKAGE__->register_method ({
    subclass => "PVE::API2::Utils",
    path => 'utils',
});

1;
